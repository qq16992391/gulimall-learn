#### problem 总结

### 1、Pubsub 使用问题解决
## 1.1 问题引入描述
brand-select.vue 中使用 Pubsub,然后报错了。是因为项目中没有安装此模块，需要安装一下，安装过程如下：

## 1.2 安装 pubsub 组件
npm install --save pubsub-js

## 1.3 导入组件 并全局挂载
```
//在 src/main.js 中导入,代码如下:
//导入组件
import PubSub from 'pubsub-js'
//全局挂载
Vue.prototype.PubSub = PubSub    //组件发布订阅消息
```


### 2、attrupdate 报 404 问题解决 
## 2.1 问题引入描述
100 集视频的时候,我看了一下,老师的操作,在 spu 管理的时候,点击规格会跳转到规格维护的页面中,但是我的没有跳转 404.

## 2.2 分析问题如下
//这个按钮的逻辑如下:
```
<el-button type="text" size="small" @click="attrUpdateShow(scope.row)">规格</el-button>
attrUpdateShow(row) {
  console.log(row);
  this.$router.push({
    path: "/product-attrupdate",
    query: { spuId: row.id, catalogId: row.catalogId }
  });
```

可以看到就是一个跳转,product 模块下的 attrupdate,正常如果 product 模块下有这个 attrupdate.vue 时就可以跳转,但是跳转失败,报了 404,然后就很纳闷.
然后百度了一通,就是这个路由没有注册上,然后分析了他的 router 下的 index.js 的代码
```
router.beforeEach((to, from, next) => {
  // 添加动态(菜单)路由
  // 1. 已经添加 or 全局路由, 直接访问
  // 2. 获取菜单列表, 添加并保存本地存储
  if (router.options.isAddDynamicMenuRoutes || fnCurrentRouteType(to, globalRoutes) === 'global') {
    next()
  } else {
    http({
      url: http.adornUrl('/sys/menu/nav'),
      method: 'get',
      params: http.adornParams()
    }).then(({data}) => {
      if (data && data.code === 0) {
        fnAddDynamicMenuRoutes(data.menuList)
        router.options.isAddDynamicMenuRoutes = true
        sessionStorage.setItem('menuList', JSON.stringify(data.menuList || '[]'))
        sessionStorage.setItem('permissions', JSON.stringify(data.permissions || '[]'))
        next({ ...to, replace: true })
      } else {
        sessionStorage.setItem('menuList', '[]')
        sessionStorage.setItem('permissions', '[]')
        next()
      }
    }).catch((e) => {
      console.log(`%c${e} 请求菜单列表和权限失败，跳转至登录页！！`, 'color:blue')
      router.push({ name: 'login' })
    })
  }
})
```

其实就是读取 menuList 然后注册路由,然后我浏览器调试了一下,确实没有这个,这就清楚了,数据库肯定没有这个菜单.然后就去数据库查询了一下,果然没有.坑呀,所以就自己手动添加了这个菜单.
```
select * FROM sys_menu WHERE name = '规格维护';//找不到记录
INSERT INTO `mall_admin`.`sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (76, 37, '规格维护', 'product/attrupdate', '', 1, 'log', 0);
```

然后重启应用,刷新就成功了。


### 3、属性修改弹窗字段信息没有回显问题解决
## 3.1 问题引入描述
修改规格参数的时候发现,值类型报错(valueType，就是值类型-只能有一个值\可以有多个值),这个值是一个 switch 开关,肯定是有值的,
但是我修改时发现没有回显过来,就追踪了一下,发现是给的 sql 脚本中没有这个字段,Vo 里有,但是 entity 里没有


## 3.2 解决
entity 里添加了 valueType 字段,然后数据库也添加了这个字段，重启应用，重新操作发现有值了

## 3.3 Invalid prop: type check failed for prop "router". Expected Boolean, got String with value "true".
//这种报错遇到了好几次,解决方案都是在添加的属性前加上 ":",比如下的 router="true",如果前面没有 : 就会报这个错
<el-menu :default-openeds="['1', '3']" :router="true">