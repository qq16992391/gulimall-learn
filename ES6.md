#### ES6 的相关总结
### 1、let、const
## 1.1 var 的变量往往会域域,let 的则是严格局部作用域
```
// var 声明的变量往往会越域
// let 声明的变量有严格局部作用域
{
    var a = 1;
    let b = 2;
}
console.log(a);  // 1
console.log(b);  // ReferenceError: b is not defined
```

## 1.2 var 可以声明多次
```
// var 可以声明多次
// let 只能声明一次
var m = 1
var m = 2
let n = 3
let n = 4
console.log(m)  // 2
console.log(n)  // Identifier 'n' has already been declared
```

## 1.3 var 会变量提升
```
// var 会变量提升
// let 不存在变量提升(没用定义就使用会报错)
console.log(x);  // undefined
var x = 10;
console.log(y);   //ReferenceError: y is not defined
let y = 20;
```
## 1.4 const 
```
// 1. 声明之后不允许改变
// 2. 一但声明必须初始化，否则会报错
const a = 1;
a = 3; //Uncaught TypeError: Assignment to constant variable.
```

### 2、结构表达式
## 2.1 数组对象赋值
```
//数组解构
//原始的方式
let arr = [1,2,3];
let a = arr[0];
let b = arr[1];
let c = arr[2];

//新的方式
let [a,b,c] = arr;
console.log(a,b,c)
```

## 2.2 对象结构赋值
```
//原始的方式
const person = {
    name: "jack",
    age: 21,
    language: ['java', 'js', 'css']
}
const name = person.name;
const age = person.age;
const language = person.language;

//新的方式
const { name, age, language } = person;//这样就相当于定义了 name、age、language 的变量并从 person 中取值赋值给他们

//下面这个就相当于,我不想取名 name,我可以换一个变量名 abc,那么下面取值的时候直接用 abc 即可。
const { name: abc, age, language } = person;
console.log(abc, age, language)
```

## 2.3 字符串扩展
```
let str = "hello.vue";
console.log(str.startsWith("hello"));//true
console.log(str.endsWith(".vue"));//true
console.log(str.includes("e"));//true
console.log(str.includes("hello"));//true
```

## 2.4 字符串模板(下面的那个符号位于键盘 ESC 下面的那个键)
```
let ss = `<div>
            <span>hello world<span>
        </div>`;
console.log(ss);
```
1)、字符串插入变量和表达式。变量名写在 ${} 中，${} 中可以放入 JavaScript 表达式。
function fun() {
    return "这是一个函数"
}

2)、这个表达式需要先用一个变量定义然后再打印,如果直接在 console.log() 中打印,解析不出来相应的值
```
let info = `我是${abc}，今年${age + 10}了, 我想说： ${fun()}`;
console.log(info);
```

### 3、函数优化
## 3.1 函数默认值
// 在ES6以前，我们无法给一个函数参数设置默认值，只能采用变通写法：
//1)、原始的方式
```
function add(a, b) {
    // 判断b是否为空，为空就给默认值1
    b = b || 1;
    return a + b;
}
// 传一个参数
console.log(add(10));
```

2)、现在的方式
//现在可以这么写：直接给参数写上默认值，没传就会自动使用默认值
```
function add2(a, b = 1) {
    return a + b;
}
console.log(add2(20));
//使用 => 的方式
let func2 = (a, b = 1) => a + b;
console.log("func2 result:" + func2(1));
```

## 3.2 不定参数
```
function fun(...values) {
    console.log(values.length)
}
fun(1, 2)      //2
fun(1, 2, 3, 4)  //4
```

##3.3 箭头函数
//1)、以前声明一个方法-现在声明一个方法
```
//例子1
var print = function (obj) {
    console.log(obj);
}
var print = obj => console.log(obj);
print("hello");

//例子 2
var sum = function (a, b) {
    c = a + b;
    return a + c;
}
var sum2 = (a, b) => a + b;
console.log(sum2(11, 12));

//例子 3
var sum3 = (a, b) => {
    c = a + b;
    return a + c;
}
console.log(sum3(10, 20))

//打印 peron 的 name,传入 person,然后输出 person.name
const person = {
    name: "jack",
    age: 21,
    language: ['java', 'js', 'css']
}
function hello(person) {
    console.log("hello," + person.name)
}
```


//箭头函数+解构
```
//这里是一个演进
var h1 = (person) => console.log("hello," + person.name);
//然后使用解构变成如下形式,可以理解为{name} 表示传递有一个有 name 属性的对象. 
var h2 = ({name}) => console.log("hello," + name);

var hello2 = ({ name }) => console.log("hello," + name);
hello2(person);
```

### 4、对象优化 
## 4.1 增加了新的方法
```
const person = {
    name: "jack",
    age: 21,
    language: ['java', 'js', 'css']
}
console.log(Object.keys(person));//["name", "age", "language"]
console.log(Object.values(person));//["jack", 21, Array(3)]
console.log(Object.entries(person));//[Array(2), Array(2), Array(2)]

const target = { a: 1 };
const source1 = { b: 2 };
const source2 = { c: 3 };
//{a:1,b:2,c:3}
Object.assign(target, source1, source2);
console.log(target);
```

## 4.2 声明对象简写
```
const age = 23
const name = "张三"
const person1 = { age: age, name: name }
//简写
const person2 = { age, name }
console.log(person2);
```

## 4.3 对象的函数属性简写
```
let person3 = {
    name: "jack",
    // 以前：
    eat: function (food) {
        console.log(this.name + "在吃" + food);
    },
    //箭头函数this不能使用，对象.属性
    //简写方式 1:
    eat2: food => console.log(person3.name + "在吃" + food),
    //简写方式 2:
    eat3(food) {
        console.log(this.name + "在吃" + food);
    }
}
person3.eat("香蕉");
person3.eat2("苹果")
person3.eat3("橘子");
```

## 4.4 对象拓展运算符
//1)、拷贝对象（深拷贝）
```
let p1 = { name: "Amy", age: 15 }
let someone = { ...p1 }
console.log(someone)  //{name: "Amy", age: 15}
```

//2)、合并对象
```
let age1 = { age: 15 }
let name1 = { name: "Amy" }
let p2 = {name:"zhangsan"}
p2 = { ...age1, ...name1 } //copy 之后会覆盖原来的值
console.log(p2)
```

### 5、map 和 reduce 
//数组中新增了map和reduce方法。
##5.1  map()：接收一个函数，将原数组中的所有元素用这个函数处理后放入新数组返回。
```
let arr = ['1', '20', '-5', '3'];
//  arr = arr.map((item)=>{
//     return item*2
//  });
arr = arr.map(item => item * 2);
console.log(arr);
```

## 5.2 reduce() 为数组中的每一个元素依次执行回调函数，不包括数组中被删除或从未被赋值的元素，
```
//[2, 40, -10, 6]
//arr.reduce(callback,[initialValue])
/**
* 1、previousValue （上一次调用回调返回的值，或者是提供的初始值（initialValue））
* 2、currentValue （数组中当前被处理的元素）
* 3、index （当前元素在数组中的索引）
* 4、array （调用 reduce 的数组）
*/
let result = arr.reduce((a, b) => {
    console.log("上一次处理后：" + a);
    console.log("当前正在处理：" + b);
    return a + b;
}, 100);
console.log(result)
```

### 6、promise 
```
//1、查出当前用户信息
//2、按照当前用户的id查出他的课程
//3、按照当前课程id查出分数
// $.ajax({
//     url: "mock/user.json",
//     success(data) {
//         console.log("查询用户：", data);
//         $.ajax({
//             url: `mock/user_corse_${data.id}.json`,
//             success(data) {
//                 console.log("查询到课程：", data);
//                 $.ajax({
//                     url: `mock/corse_score_${data.id}.json`,
//                     success(data) {
//                         console.log("查询到分数：", data);
//                     },
//                     error(error) {
//                         console.log("出现异常了：" + error);
//                     }
//                 });
//             },
//             error(error) {
//                 console.log("出现异常了：" + error);
//             }
//         });
//     },
//     error(error) {
//         console.log("出现异常了：" + error);
//     }
// });


//1、Promise可以封装异步操作
// let p = new Promise((resolve, reject) => {
//     //1、异步操作
//     $.ajax({
//         url: "mock/user.json",
//         success: function (data) {
//             console.log("查询用户成功:", data)
//             resolve(data);
//         },
//         error: function (err) {
//             reject(err);
//         }
//     });
// });

// p.then((obj) => {
//     return new Promise((resolve, reject) => {
//         $.ajax({
//             url: `mock/user_corse_${obj.id}.json`,
//             success: function (data) {
//                 console.log("查询用户课程成功:", data)
//                 resolve(data);
//             },
//             error: function (err) {
//                 reject(err)
//             }
//         });
//     })
// }).then((data) => {
//     console.log("上一步的结果", data)
//     $.ajax({
//         url: `mock/corse_score_${data.id}.json`,
//         success: function (data) {
//             console.log("查询课程得分成功:", data)
//         },
//         error: function (err) {
//         }
//     });
// })

// function get(url, data) {
//     return new Promise((resolve, reject) => {
//         $.ajax({
//             url: url,
//             data: data,
//             success: function (data) {
//                 resolve(data);
//             },
//             error: function (err) {
//                 reject(err)
//             }
//         })
//     });
// }
let get = (url, data) => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: url,
            data: data,
            success: function (data) {
                resolve(data);
            },
            error: function (err) {
                reject(err)
            }
        })
    })
};

get("mock/user.json")
.then((data) => {
    console.log("用户查询成功~~~:", data)
    return get(`mock/user_corse_${data.id}.json`);
})
.then((data) => {
    console.log("课程查询成功~~~:", data)
    return get(`mock/corse_score_${data.id}.json`);
})
.then((data) => {
    console.log("课程成绩查询成功~~~:", data)
})
.catch((err) => {
    console.log("出现异常", err)
});
```

### 7、模块化

```
//export import 
//hello.js
//这种情况下因为定义了名字，就是 util,那么 import 如下:import util from "./hello.js"
// export const util = {
//     sum(a, b) {
//         return a + b;
//     }
// }

//这种情况下导入的时候可以随便起名比如：import abc from './hello.js'
export default {
    sum(a, b) {
        return a + b;
    }
}

//`export`不仅可以导出对象，一切JS变量都可以导出。比如：基本类型变量、函数、数组、对象。
//main.js
import abc from "./hello.js"
import {name,add} from "./user.js"
```


